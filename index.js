// import App from './src/javascript/app';
// import './src/styles/styles.css';

// new App();

//global variables
const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
const fightersDetailsMap = new Map();
const chooseFighters = document.forms[0]
const inputs = document.getElementsByClassName('fighter-name')

// displaying health and winner
  const winner = document.getElementById("winnerMessage")
  let fighterOneHealthMessage = document.getElementsByClassName('fighterHealth')[0]
  let fighterTwoHealthMessage = document.getElementsByClassName('fighterHealth')[1]

// The array that will contain user selected fighters
let fightersList = []
const startButton = document.getElementById('start')
const submitFighters = document.getElementsByClassName("submitFighters")[0]
  submitFighters.onclick = (event) => {

    // Preventing the page from reloading on submiting fighters
  event.preventDefault()
  list()
  if(fightersList.length == 2) {
    startGame()
    loadingElement.style.visibility = 'visible'
    startButton.style.visibility = 'visible'
  }
}
//Function for selecting fighters
function list() {
  [...inputs].forEach(el => {
    if(el.checked) {
      fightersList.push(el.value)
    }
  })
  // Checking if exactly two fighters have been selected
    if(fightersList.length != 2) {
      alert("You must select 2 fighters 😱")
      fightersList = []
    }
    //Connecting the selected fighters to fighters' objects with their information
  this[fightersList[0]] = eval(fightersList[0])
  this[fightersList[1]] = eval(fightersList[1])
  const indOne = this[fightersList[0]]
  const indTwo = this[fightersList[1]]
  const fighterOneDisplay = document.getElementsByClassName('fighter')[indOne.index]
  const fighterTwoDisplay = document.getElementsByClassName('fighter')[indTwo.index]

  //Events after the start button has been pressed
  startButton.onclick = () => {

    // Displaying the chosen fighters
    fighterOneDisplay.style.setProperty('display', 'inline-block')
    fighterTwoDisplay.style.setProperty('display', 'inline-block')

    //Turning the second fighter to face the first
    fighterTwoDisplay.firstChild.style.setProperty('transform', 'scale(-1, 1)')

    //Displaying the hit buttons and hiding the start game button
    loadingElement.style.visibility = 'hidden'
    const buttons = document.getElementsByClassName("fighterHit")
    Array.from(buttons).forEach(el => el.classList.add('display'))
    startButton.style.visibility = 'hidden'
  }
  // Passing chosen fighters as arguements to the fight function
  return fight(this[fightersList[0]], this[fightersList[1]])
}
// The start game screen with overlay and start game button
  function startGame() {
    chooseFighters.style.setProperty('opacity', 0)
    const readyAudio = document.getElementById('audio-ready')
    readyAudio.volume = 0.1
    readyAudio.play()
    const fightersLine = document.getElementsByClassName('fighter')
    Array.from(fightersLine).forEach(el => el.style.setProperty('display', 'none'))
  }

// Making a promise to fetch fighters' information
function callApi(endpoind, method) {
  const url = API_URL + endpoind
  const options = {
    method
  };

  return fetch(url, options)
    .then(response => 
      response.ok 
        ? response.json() 
        : Promise.reject(Error('Failed to load'))
    )
    .catch(error => { throw error });
}

// Fetching fighters' information from the API
class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint);

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
}
  async getFighterDetails(_id) {
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint);

      return JSON.parse(atob(apiResult.content));
    } catch(error) {
      throw error;
    }
  }
}
// Initializing the FighterService class to use its methods
const fighterService = new FighterService();

// Building the fighters layout
class View {
  element;

  createElement({ tagName, className = '', attributes = {} }) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'p', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

class FightersView extends View {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
    chooseFighters.classList.add('display')
  }

//Handling clicks on each fighter
  handleFighterClick(event, fighter) {
    const element = event.target

    //Receiving the info about fighter and storing it in the map collection

    // Info modal
    const modal = document.getElementById('fighterInfoDiv')
    element.onclick = () => {
      const fighterInfo = fighterService.getFighterDetails(fighter._id).then(value => {
        modal.childNodes[1].innerHTML = `name: ${value.name} <br>
      health: <span>${value.health}</span> <br>
      attack: <span>${value.attack}</span> <br>
      defense: <span>${value.defense}</span><br>`
      fightersDetailsMap.set(fighter._id, value)
      const el = fightersDetailsMap.get(fighter._id)
      modal.classList.add('display')
      })
    }
      //Button to close the modal
      const closeButton = document.getElementById('close-button')
      closeButton.onclick = () => {
        modal.classList.remove('display')
      }
    }
  }

// Fighter class
class Fighter {
  constructor(name, health, attack, defense, index) {
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
    this.index = index;
  }

  // Fighter methods
  getHitPower() {
    let criticalHitChange = Math.round(Math.random() + 1)
    const power = this.attack * criticalHitChange;
    return power;
  }
  getBlockPower() {
    let dodgeChance = Math.round(Math.random() + 1)
    const power = this.defense * dodgeChance;
    return power;
  }
}
//Fighter instantiation
const ryu = new Fighter("Ryu", 45, 4, 3, 0)
const dhalsim = new Fighter("Dhalsim", 60, 3, 1, 1)
const guile = new Fighter('Guile', 45, 4, 3, 2)
const zangief = new Fighter('Zangief', 60, 4, 1, 3)
const ken = new Fighter('Ken', 45, 3, 4, 4)
const bison = new Fighter('Bison', 45, 5, 4, 5)
const fightersArray = [ryu, dhalsim, guile, zangief, ken, bison]

// fight function!!!_________________________________________________________________________

function fight(fighterOne, fighterTwo) {

  // Audios
  const audio = document.getElementById('audio')
  const victoryAudio = document.getElementById('audio-victory')
  audio.volume = 0.05
  victoryAudio.volume = 0.15

// Health of the fighters
  function fighterOneHealth() {
    let loss = fighterTwo.getHitPower() - fighterOne.getBlockPower()
    loss < 0 ? fighterOne.health : fighterOne.health = fighterOne.health - loss
    fighterOneHealthMessage.innerText = `${fighterOne.name} health: ${fighterOne.health}`
    return fighterOne.health
  }
  function fighterTwoHealth() {
    let loss = fighterOne.getHitPower() - fighterTwo.getBlockPower()
    loss < 0 ? fighterTwo.health : fighterTwo.health = fighterTwo.health - loss
    fighterTwoHealthMessage.innerText = `${fighterTwo.name} health: ${fighterTwo.health}`
    return fighterTwo.health
  }
  // Hit buttons
  function hitting(func1, func2) {
    const hitOne = document.getElementsByClassName('fighterOneHit')[0]
    const hitTwo = document.getElementsByClassName('fighterTwoHit')[0]
    hitOne.onclick = () => {
      return hitFunction(fighterTwoHealth)
      }
    hitTwo.onclick = () => {
      return hitFunction(fighterOneHealth)
      }

// Health change on hit
    const hitFunction = (warriorHealth) => fighterOne.health > 0 && fighterTwo.health > 0 ?
    warriorHealth() && audio.play() : winnerMessage() && victoryAudio.play()

// Displaying a winner
    const winnerMessage = () => {
    fighterOne.health > fighterTwo.health ? winner.innerText = fighterOne.name + " wins!":
    winner.innerText = fighterTwo.name + " wins!"

    // Displaying the reload button
    startButton.style.visibility = 'visible'
    startButton.innerText = 'Play again'
    startButton.style.setProperty('left', '40vw')
    startButton.onclick = () => history.go(0)
    //Hiding the hit buttons after the game is over
    const buttons = document.getElementsByClassName("fighterHit")
    Array.from(buttons).forEach(el => el.style.visibility = 'hidden')
    return winnerMessage
  }
  }
  // Passing fighters' health into hitting function
  return hitting(fighterOneHealth, fighterTwoHealth)

}
class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

new App();
